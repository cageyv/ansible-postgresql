# postgresql

An Ansible role for installing and managing PostgreSQL servers on Debian based
systems.


## Requirements

* Debian 9
* Ansible >= 2.4

## Role Variables

- `postgresql_version`: PostgreSQL version to install. On Debian-based
  platforms, the default is 9.6.

- `postgresql_conf`: A hash (dictionary) of `postgresql.conf` options and
  values. These options are not added to `postgresql.conf` directly - the role
  adds a `conf.d` subdirectory in the configuration directory and an include
  statement for that directory to `postgresql.conf`. Options set in
  `postgresql_conf` are then set in `conf.d/25ansible_postgresql.conf`.

  Due to YAML parsing, you must take care when defining values in
  `postgresql_conf` to ensure they are properly written to the config file. For
  example:

  ```yaml
  postgresql_conf:
    max_connections: 250
    archive_mode: "off"
    work_mem: "'8MB'"
  ```

  Becomes the following in `25ansible_postgresql.conf`:

  ```
  max_connections = 250
  archive_mode = off
  work_mem: '8MB'
  ```

- `postgresql_pg_hba_conf`: A list of lines to add to `pg_hba.conf`

- `postgresql_pg_hba_local_postgres_user`: If set to `false`, this will remove
  the `postgres` user's entry from `pg_hba.conf` that is preconfigured on
  Debian-based PostgreSQL installations. You probably do not want to do this
  unless you know what you're doing.

- `postgresql_pg_hba_local_socket`: If set to `false`, this will remove the
  `local` entry from `pg_hba.conf` that is preconfigured by the PostgreSQL
  package.

- `postgresql_pg_hba_local_ipv4`: If set to `false`, this will remove the `host
  ... 127.0.0.1/32` entry from `pg_hba.conf` that is preconfigured by the
  PostgreSQL package.

- `postgresql_pg_hba_local_ipv6`: If set to `false`, this will remove the `host
  ... ::1/128` entry from `pg_hba.conf` that is preconfigured by the PostgreSQL
  package.

- `postgresql_pgdata_dir`: Only set this if you have changed the `$PGDATA`
  directory from the package default. Note this does not configure PostgreSQL
  to actually use a different directory, you will need to do that yourself, it
  just allows the role to properly locate the directory.

- `postgresql_conf_dir`: As with `postgresql_pgdata_dir` except for the
  configuration directory.
- `postgresql_users` - List of users to be created (optional)
    ```
    postgresql_users:
      - name: baz
        pass: pass
        encrypted: no               # denotes if the password is already encrypted.
    ```

- `postgresql_user_privileges`: - List of user privileges to be applied (optional)
    ```
    postgresql_user_privileges:
      - name: baz                   # user name
        db: foobar                  # database
        priv: "ALL"                 # privilege string format: example: INSERT,UPDATE/table:SELECT/anothertable:ALL
        role_attr_flags: "CREATEDB" # role attribute flags
    ```

- `postgresql_databases`:  List of databases to be created (optional)
    ```
    postgresql_databases:
      - name: foobar
        owner: baz          # optional; specify the owner of the database
        hstore: yes         # flag to install the hstore extension on this database (yes/no)
        uuid_ossp: yes      # flag to install the uuid-ossp extension on this database (yes/no)
        citext: yes         # flag to install the citext extension on this database (yes/no)
        encoding: 'UTF-8'   # override global {{ postgresql_encoding }} variable per database
        lc_collate: 'en_GB.UTF-8'   # override global {{ postgresql_locale }} variable per database
        lc_ctype: 'en_GB.UTF-8'     # override global {{ postgresql_ctype }} variable per database
    ```

- `postgresql_admin_user`: The admin user used by the role, defaults to `postgres`

- `postgresql_port`: The port that the role will use to connect to the
  database, defaults to `5432`. note this does not configure PostgreSQL to
  actually use a different port, you will need to do that yourself, it just
  allows the role to connect to the db for doing operations.

### Other variables:

```
  postgresql_encoding: 'UTF-8'
  postgresql_locale: 'en_US.UTF-8'
  postgresql_ctype: 'en_US.UTF-8'
```


## Example Playbook

```yaml
- hosts: servers
  vars:
    example_var: foovar
  roles:
    - postgresql
```

License
-------

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

Author Information
------------------

Inspired by:

* https://github.com/galaxyproject/ansible-postgresql
* https://github.com/ANXS/postgresql

```
Abel Luck <abel@guardianproject.info>
Guardian Project https://guardianproject.info
```

